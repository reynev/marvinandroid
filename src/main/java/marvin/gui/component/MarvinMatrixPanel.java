/**
 MarvinAndroid Project <2014>

 Initial version by:

 Marcin Piłat

 site: https://bitbucket.org/reynev/marvinandroid

 GPL
 Copyright (C) <2014>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package marvin.gui.component;

import android.content.Context;
import android.text.InputType;
import android.widget.EditText;
import android.widget.GridLayout;

/**
 * Components for matrix attributes input.
 *
 * Created on 08.04.14.
 * @author reynev
 *
 * Based on MarvinMatrixPanel from Marvin Project (marvinproject.sourceforge.net/)
 */
public class MarvinMatrixPanel extends GridLayout {

    private EditText textFields[];
    private int rows;
    private int	columns;


    public MarvinMatrixPanel(Context context) {
        this(context, 3, 3);
    }

    public MarvinMatrixPanel(Context context, int rows, int columns) {
        super(context);
        this.rows = rows;
        this.columns = columns;
        setColumnCount(3);
        setRowCount(3);
        GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
        layoutParams.width = GridLayout.LayoutParams.MATCH_PARENT;
        setLayoutParams(layoutParams);

        textFields = new EditText[rows*columns];
        for(int r = 0 ; r < rows ; ++r){
            for(int c = 0 ; c < columns ; ++c){
                EditText editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_NUMBER_FLAG_SIGNED);
                addView(editText);
                editText.setText(0+"");
                textFields[r*columns+c] = editText;
            }
        }

    }

    public double[][] getValue(){
        double[][] result = new double[rows][columns];
        for(int r=0; r<rows; r++){
            for(int c=0; c<columns; c++){
                result[r][c] = Double.parseDouble(textFields[columns*r + c].getText().toString());
            }
        }
        return result;
    }

    public void populateMatrix(double[][] matrix){
        for(int r=0; r<rows; r++){
            for(int c=0; c<columns; c++){
                textFields[columns*r + c].setText(matrix[r][c] + "");
            }
        }
    }
}
