/**
 MarvinAndroid Project <2014>

 Initial version by:

 Marcin Piłat

 site: https://bitbucket.org/reynev/marvinandroid

 GPL
 Copyright (C) <2014>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package marvin.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.LinkedHashMap;

import marvin.gui.component.MarvinMatrixPanel;
import marvin.util.MarvinAttributes;

import static marvin.gui.MarvinPluginWindowComponent.ComponentType.COMPONENT_CHECKBOX;
import static marvin.gui.MarvinPluginWindowComponent.ComponentType.COMPONENT_COMBOBOX;
import static marvin.gui.MarvinPluginWindowComponent.ComponentType.COMPONENT_MATRIX_PANEL;
import static marvin.gui.MarvinPluginWindowComponent.ComponentType.COMPONENT_SLIDER;
import static marvin.gui.MarvinPluginWindowComponent.ComponentType.COMPONENT_TEXTAREA;
import static marvin.gui.MarvinPluginWindowComponent.ComponentType.COMPONENT_TEXTFIELD;

/**
 * ViewGroup with input attributes for MarvinPlugin.
 *
 * Created on 07.04.14
 * @author reynev
 *
 * Based on MarvinAttributesPanel from Marvin Project (marvinproject.sourceforge.net/)
 */
public class MarvinAttributesPanel extends LinearLayout {

    private LinearLayout actualRow;
    private LinkedHashMap<String, MarvinPluginWindowComponent> componentsMap = new LinkedHashMap();

    public MarvinAttributesPanel(Context context) {
        super(context);
        setOrientation(VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        setLayoutParams(params);
        newComponentRow(context);
    }

    @Override
    protected void onLayout(boolean b, int i, int i2, int i3, int i4) {
        super.onLayout(b, i, i2, i3, i4);
    }

    /**
     * Adds new component Line
     */
    public void newComponentRow(Context c) {
        actualRow = new LinearLayout(c);
        actualRow.setOrientation(HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        actualRow.setLayoutParams(params);
        this.addView(actualRow);
    }

    /**
     * Gets actual component line
     *
     * @return
     */
    public LinearLayout getCurrentComponentRow() {
        return actualRow;
    }

    /**
     * This method is useful if the developer need to add an external component.
     *
     * @return current panel.
     */
    public ViewGroup getCurrentPanel() {
        return this;
    }

    public void plugComponent(View comp) {
        actualRow.addView(comp);
    }

    /**
     * Adds new component
     *
     * @param id
     * @param comp
     * @param attrID
     * @param attr
     * @param type
     */
    public void plugComponent(String id, View comp, String attrID, MarvinAttributes attr,
                              MarvinPluginWindowComponent.ComponentType type) {
        actualRow.addView(comp);
        componentsMap.put(id, new MarvinPluginWindowComponent(id, attrID, attr, comp, type));
    }

    /**
     * Returns a component by its id.
     * @param compID MarvinPluginWindowComponent id.
     */
    public MarvinPluginWindowComponent getComponent(String compID){
		return componentsMap.get(compID);
	}

    /**
     * Adds label
     *
     * @param id   component id.
     * @param text label text attribute.
     * @param c    android Context Object.
     */
    public void addLabel(String id, String text, Context c) {
        TextView textView = new TextView(c);
        textView.setText(text);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(params);
        actualRow.addView(textView);
    }

    /**
     * Adds image
     *
     * @param id  component id.
     * @param img image to be displayed.
     */
    public void addImage(String id, Bitmap img, Context c) {
        ImageView imageView = new ImageView(c);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        imageView.setLayoutParams(params);
        imageView.setImageBitmap(img);
        actualRow.addView(imageView);
    }

    /**
     * Adds TextField
     *
     * @param id     component id.
     * @param attrID attribute id.
     * @param attr   MarivnAttributes Object.
     * @param c      android Context Object.
     */
    public void addTextField(String id, String attrID, MarvinAttributes attr, Context c) {
        EditText editText = new EditText(c);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        editText.setLayoutParams(params);
        plugComponent(id, editText, attrID, attr, COMPONENT_TEXTFIELD);
    }

    /**
     * @param id      - component id.
     * @param attrID  - attribute id.
     * @param lines   - number of lines.
     * @param columns - number of columns.
     * @param attr    - MarivnAttributes Object.
     * @param c       - android Context Object.
     */
    public void addTextArea(String id, String attrID, int lines, int columns, MarvinAttributes attr, Context c) {
        EditText editText = new EditText(c);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        editText.setLayoutParams(params);
        plugComponent(id, editText, attrID, attr, COMPONENT_TEXTAREA);
    }

    /**
     * Add ComboBox
     *
     * @param id     component's id.
     * @param attrID attribute id.
     * @param items  items array.
     * @param attr   MarvinAttributes object.
     * @param c      android Context Object.
     */
    public void addComboBox(String id, String attrID, Object[] items, MarvinAttributes attr, Context c) {
        Spinner spinner = new Spinner(c);

        ArrayAdapter<Object> adapter = new ArrayAdapter(c,android.R.layout.simple_spinner_item,items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        plugComponent(id, spinner, attrID, attr, COMPONENT_COMBOBOX);
    }

    /**
     * Adds Slider
     *
     * @param id          component id.
     * @param attrID      attribute id.
     * @param orientation slider orientation
     * @param a_min       minimum value.
     * @param a_max       maximum value.
     * @param a_value     initial value.
     * @param attr        MarvinAttributes object
     */
    protected void addSlider(String id, String attrID, int orientation, int a_min, int a_max, int a_value, MarvinAttributes attr, Context c) {
        LinearLayout sliderGroup = new LinearLayout(c);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        sliderGroup.setLayoutParams(layoutParams);

        final TextView textView = new TextView(c);
        LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(tvParams);
        textView.setText(a_value + "");

        SeekBar seekBar = new SeekBar(c);
        LinearLayout.LayoutParams sliderParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        seekBar.setLayoutParams(sliderParams);
        if (orientation == VERTICAL) {
            seekBar.setRotation(270);
            sliderGroup.setOrientation(HORIZONTAL);
        } else {
            sliderGroup.setOrientation(VERTICAL);
        }

        seekBar.setMax(a_max-a_min);
        seekBar.setProgress(a_value - a_min);
        final int min = a_min;

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textView.setText(i + min + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        sliderGroup.addView(textView);
        sliderGroup.addView(seekBar);

        actualRow.addView(sliderGroup);
        /*actualRow.addView(textView);
        actualRow.addView(seekBar);*/
        MarvinPluginWindowComponent sliderMC = new MarvinPluginWindowComponent(id, attrID, attr, seekBar,
               COMPONENT_SLIDER);
        sliderMC.setMin(a_min);
        componentsMap.put(id, sliderMC);
    }

    /**
     * Add HorizontalSlider
     *
     * @param id     component ID.
     * @param attrID attribute ID.
     * @param min    minimum value.
     * @param max    maximum value.
     * @param value  initial value.
     * @param attr   MarvinAttributes object
     */
    public void addHorizontalSlider(String id, String attrID, int min, int max, int value, MarvinAttributes attr, Context c) {
        addSlider(id, attrID, HORIZONTAL, min, max, value, attr, c);
    }


    /**
     * Add VerticalSlider
     *
     * @param id     component ID
     * @param attrID attribute ID
     * @param min    minimum value
     * @param max    maximum value
     * @param value  initial value
     * @param attr   MarvinAttributes object
     */
    public void addVerticalSlider(String id, String attrID, int min, int max, int value, MarvinAttributes attr, Context c) {
        addSlider(id, attrID, VERTICAL, min, max, value, attr, c);
    }

    /**
     * Add CheckBox
     *
     * @param id     component ID
     * @param cbText CheckBox text attribute
     * @param attrID attribute ID
     * @param attr   MarvinAttributes object
     */
    public void addCheckBox(String id, String cbText, String attrID, MarvinAttributes attr, Context c) {
        CheckBox checkBox = new CheckBox(c);
        checkBox.setText(cbText);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        checkBox.setLayoutParams(params);
        plugComponent(id, checkBox, attrID, attr, COMPONENT_CHECKBOX);
    }

    /**
     * Add ButtonGroup
     */
    /*public void addButtonGroup(String id, String attrID, MarvinAttributes attr) {
        //TODO
    }*/


    /**
     * @param id      component ID
     * @param attrID  attribute ID
     * @param attr    MarvinAttributes object
     * @param rows    number of rows
     * @param columns number of columns
     */
    public void addMatrixPanel(String id, String attrID, MarvinAttributes attr, int rows, int columns, Context c) {
        MarvinMatrixPanel marvinMatrixPanel = new MarvinMatrixPanel(c,rows,columns);
        plugComponent(id,marvinMatrixPanel,attrID,attr,COMPONENT_MATRIX_PANEL);
    }

    /**
     * Update the attributes value based on the associated components.
     */
    public void applyValues() {
        for( LinkedHashMap.Entry entry : componentsMap.entrySet()){
            MarvinPluginWindowComponent component = (MarvinPluginWindowComponent) entry.getValue();
            component.getAttributes().set(component.getAttributeID(),getValue(component));
        }
    }

    /**
     * Converts a string to the attribute type.
     *
     * @param value attribute new value.
     * @param type  attribute old value.
     * @return parsed value as previous one.
     */
    public Object stringToType(String value, Object type) {
        if(type == null){
            type = "";
        }
        Class<?> l_class = type.getClass();
        if (l_class == Double.class) {
            return Double.parseDouble(value);
        } else if (l_class == Float.class) {
            return Float.parseFloat(value);
        } else if (l_class == Integer.class) {
            return Integer.parseInt(value);
        } else if (l_class == String.class) {
            return value.toString();
        } else if (l_class == Boolean.class) {
            return Boolean.parseBoolean(value);
        }
        return null;
    }

    /**
     * @return the value associated with the specified component
     */
    public Object getValue(MarvinPluginWindowComponent marvinComponent) {
		MarvinAttributes attr = marvinComponent.getAttributes();
        View component = marvinComponent.getComponent();

		switch(marvinComponent.getType()){
			case COMPONENT_TEXTFIELD:
            case COMPONENT_TEXTAREA:
				return stringToType( ((EditText)component).getText().toString(), attr.get(marvinComponent.getAttributeID()));
			case COMPONENT_COMBOBOX:
				return ( ((Spinner)component).getSelectedItem());
			case COMPONENT_SLIDER:
				return ( ((SeekBar)component).getProgress() + marvinComponent.getMin());
			case COMPONENT_CHECKBOX:
				return ( ((CheckBox)component).isChecked());
			case COMPONENT_MATRIX_PANEL:
				return  ( ((MarvinMatrixPanel)component).getValue());
		}
        return null;
    }
}
