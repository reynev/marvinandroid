/**
 MarvinAndroid Project <2014>

 Initial version by:

 Marcin Piłat

 site: https://bitbucket.org/reynev/marvinandroid

 GPL
 Copyright (C) <2014>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package marvin.gui;

import android.view.View;

import marvin.util.MarvinAttributes;

/**
 * Generic component for PluginWindow.
 *
 * Created on 07.04.14
 * @author reynev
 *
 * Based on MarvinPluginWindowComponent from Marvin Project (marvinproject.sourceforge.net/)
 */
public class MarvinPluginWindowComponent{

    public static enum ComponentType{
        COMPONENT_TEXTFIELD, COMPONENT_SLIDER, COMPONENT_COMBOBOX,
        COMPONENT_LABEL, COMPONENT_IMAGE, COMPONENT_TEXTAREA, COMPONENT_CHECKBOX,
        COMPONENT_MATRIX_PANEL
    }

    protected String id;
    protected String attributeID;
    protected MarvinAttributes attributes;
    protected View component;
    ComponentType type;
    protected int min;

    /**
     * Constructs a new {@link MarvinPluginWindowComponent}
     * @param id component ID
     * @param attrID attribute ID
     * @param attr {@link MarvinAttributes}
     * @param comp {@link android.view.View}
     * @param type {@link marvin.gui.MarvinPluginWindowComponent.ComponentType}
     */
    public MarvinPluginWindowComponent(String id, String attrID, MarvinAttributes attr, View comp, ComponentType type){
        this.id = id;
        attributeID = attrID;
        attributes = attr;
        component = comp;
        this.type = type;
    }

    /**
     * Returns the components ID.
     * @return the components ID
     */
    public String getID(){
        return id;
    }

    /**
     * Returns the ID of the attribute associated with the component.
     * @return attributes ID.
     */
    public String getAttributeID(){
        return attributeID;
    }

    /**
     * Returns Atribute objects reference.
     * @return MarvinAttribute reference.
     */
    public MarvinAttributes getAttributes(){
        return attributes;
    }

    /**
     * Returns the swing component representation of this component.
     * @return the Swing component
     */
    public View getComponent(){
        return component;
    }

    /**
     * Returns the type of the component.
     * @return the components type.
     */
    public ComponentType getType(){
        return type;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
}