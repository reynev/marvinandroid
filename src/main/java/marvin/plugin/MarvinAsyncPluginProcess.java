package marvin.plugin;

import android.os.AsyncTask;
import android.widget.ImageView;

import marvin.image.MarvinImage;
import marvin.util.history.MarvinHistory;

/**
 * Runs long-term processes on async task in order to not blocking UI. After processing updates
 * specified ImageView with resultImage.
 *
 * Created on 15.04.14.
 * @author reynev
 */
public class MarvinAsyncPluginProcess extends AsyncTask {

    private MarvinAbstractImagePlugin plugin;
    private MarvinImage imageIn;
    private MarvinImage imageOut;

    public MarvinAsyncPluginProcess(MarvinAbstractImagePlugin plugin, MarvinImage imageIn,
                                    MarvinImage imageOut) {
        this.plugin = plugin;
        this.imageIn = imageIn;
        this.imageOut = imageOut;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        plugin.process(imageIn, imageOut);
        imageOut.update();
        return null;
    }

    public MarvinAbstractImagePlugin getPlugin() {
        return plugin;
    }

    public MarvinImage getImageIn() {
        return imageIn;
    }

    public MarvinImage getImageOut() {
        return imageOut;
    }
}
