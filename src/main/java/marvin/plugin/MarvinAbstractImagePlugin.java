/**
 Marvin Project <2007-2009>

 Initial version by:

 Danilo Rosetto Munoz
 Fabio Andrijauskas
 Gabriel Ambrosio Archanjo

 site: http://marvinproject.sourceforge.net

 GPL
 Copyright (C) <2007>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package marvin.plugin;

import android.content.Context;

import marvin.image.MarvinImage;
import marvin.image.MarvinImageMask;
import marvin.util.MarvinAttributes;

/**
 * MArvinPlugin stub to extend by image processing plugin
 */
public abstract class MarvinAbstractImagePlugin implements MarvinImagePlugin {

    private boolean valid;
    protected MarvinAttributes marvinAttributes;

    public MarvinAbstractImagePlugin(){
        marvinAttributes = new MarvinAttributes();
    }

    /**
     * Executes the algorithm.
     *
     * @param imgIn  input image.
     * @param imgOut output image.
     */
    public void process(MarvinImage imgIn, MarvinImage imgOut, MarvinImageMask mask) {
        process(imgIn, imgOut, null, mask, false);
    }

    public void process(MarvinImage imgIn, MarvinImage imgOut, MarvinAttributes attrOut) {
        process(imgIn, imgOut, attrOut, MarvinImageMask.NULL_MASK, false);
    }

    /**
     * Executes the algorithm.
     *
     * @param imgIn  input image.
     * @param imgOut output image.
     */
    public void process(MarvinImage imgIn, MarvinImage imgOut) {
        process(imgIn, imgOut, null, MarvinImageMask.NULL_MASK, false);
    }


    /**
     * Ensures that this plug-in is working consistently to its attributes.
     */
    public void validate() {
        valid = true;
    }

    /**
     * Invalidate this plug-in. It means that the attributes were changed and the plug-in needs to check whether
     * or not change its behavior.
     */
    public void invalidate() {
        valid = false;
    }

    /**
     * Determines whether this plug-in is valid. A plug-in is valid when it is correctly configured given a set
     * of attributes. When an attribute is changed, the plug-in becomes invalid until the method validate() is
     * called.
     *
     * @return
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * @return MarvinAttributes object associated with this plug-in
     */
    public MarvinAttributes getAttributes() {
        return marvinAttributes;
    }

    /**
     * Set an attribute
     *
     * @param label attributes name
     * @param value attributes value
     */
    public void setAttribute(String label, Object value) {
        marvinAttributes.set(label, value);
    }

    /**
     * Set a list of attributes. Format: (String)name, (Object)value...
     */
    public void setAttributes(Object... params) {
        marvinAttributes.set(params);
    }

    /**
     * @param label atributes name
     * @return the attributes value
     */
    public Object getAttribute(String label) {
        return marvinAttributes.get(label);
    }

}
