/**
 MarvinAndroid Project <2014>

 Initial version by:

 Marcin Piłat

 site: https://bitbucket.org/reynev/marvinandroid

 GPL
 Copyright (C) <2014>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package marvin.util.history;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import marvin.image.MarvinImage;
import marvin.plugin.MarvinAbstractImagePlugin;
import marvin.util.MarvinAttributes;

/**
 * Class that allows to create a history of used plugins, and it's results.
 * It store images and creates view with entries to display or save to file.
 * Created on 14.04.14.
 * @author reynev
 */
public class MarvinHistory{
    // Constants
    private final static int ATTRIBUTES_MARGIN = 200;
    private final static int THUMBNAIL_WIDTH = 200;

    private LinkedList<MarvinHistoryEntry> entryList;

    public MarvinHistory(){
        entryList = new LinkedList<MarvinHistoryEntry>();
    }

    public int size(){
        return entryList.size();
    }

    /**
     * Returns last entry in history, and removes it.
     * @return last entry
     */
    public MarvinHistoryEntry popLastEntry(){
        if(entryList.size() > 0){
            entryList.removeLast();
        }
        if(entryList.size() > 0){
            return entryList.getLast();
        }else {
            return new MarvinHistoryEntry(null,null, null);
        }
    }

    /**
     * Add a new entry to the history.
     *
     * @param plg Plugin class
     * @param img The modified {@code MarvinImage}
     * @param attr The {@code MarvinAttributes} applied
     *
     * @see marvin.image.MarvinImage
     * @see marvin.util.MarvinAttributes
     */
    public void addEntry(Class plg, MarvinImage img, MarvinAttributes attr){
        MarvinHistoryEntry entry = new MarvinHistoryEntry(attr,img,plg);
        entryList.add(entry);
    }

    /**
     * Add a new entry to the history.
     *
     * @param plugin Plugin object {@code MarvinAbstractImagePlugin}
     * @param img The modified {@code MarvinImage}
     *
     * @see marvin.plugin.MarvinAbstractImagePlugin
     * @see marvin.image.MarvinImage
     */
    public void addEntry(MarvinAbstractImagePlugin plugin, MarvinImage img){
        MarvinHistoryEntry entry = new MarvinHistoryEntry(plugin.getAttributes(),img,plugin.getClass());
        entryList.add(entry);
    }

    /**
     * Creates view with history as LinearLayout in ScrollView with result images
     * @param context - activity context
     * @return view with history
     */
    public ScrollView getThumbnailHistory(Context context){
        ScrollView scrollView = new ScrollView(context);
        final LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER);
        LayoutTransition layoutTransition = new LayoutTransition();
        linearLayout.setLayoutTransition(layoutTransition);
        scrollView.addView(linearLayout);
        for(MarvinHistoryEntry entry : entryList){
            ImageView imgView = new ImageView(context);
            imgView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            imgView.setImageBitmap(entry.getImage().getBitmap());
            imgView.setPadding(5, 0, 5, 10);
            TextView textView = new TextView(context);
            textView.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setText(getEntryAbout(entry));
            textView.setPadding(5, 0, 5, 10);
            textView.setVisibility(View.GONE);

            imgView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    View textView = linearLayout.getChildAt(linearLayout.indexOfChild(view)+1);
                    int visibility = textView.getVisibility();
                    if(visibility == View.GONE){
                        textView.setVisibility(View.VISIBLE);
                    }else{
                        textView.setVisibility(View.GONE);
                    }
                }
            });

            linearLayout.addView(imgView);
            linearLayout.addView(textView);
        }

        return scrollView;
    }

    /**
     * Clear the history.
     */
    public void clear(){
        entryList.clear();
    }

    private String getEntryAbout(MarvinHistoryEntry entry){
        StringBuilder about = new StringBuilder();
        if(entry.getPlugin() != null){
            about.append(entry.getPlugin().getSimpleName());
        }
        if(entry.getAttributes() != null){
            boolean odd = true;
            for(String attributeStr : entry.getAttributes().toStringArray()){
                if (odd){
                    about.append("\n");
                }else{
                    about.append(" - ");
                }
                odd = !odd;
                about.append(attributeStr);
            }
        }
        return about.toString();
    }

    /**
     * Export history as image to file
     * @param file - file to save history
     * @param context -activity context to render View
     */
    public void exportAsImage(File file, Context context) throws IOException {
        exportAsImage(file,getThumbnailHistory(context));
    }

    /**
     * Export history as image to png file
     * @param file - file to save history
     * @param historyView - history to save
     */
    public void exportAsImage(File file, ScrollView historyView) throws IOException {
        LinearLayout linearLayout = (LinearLayout) historyView.getChildAt(0);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        for(int i = 0 ; i < linearLayout.getChildCount() ; ++i){
            View view = linearLayout.getChildAt(i);
            view.setVisibility(View.VISIBLE);
        }

        //Drawing View on bitmap
        historyView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        Bitmap historyBitmap = Bitmap.createBitmap(historyView.getMeasuredWidth(), historyView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(historyBitmap);
        historyView.layout(0, 0, historyView.getMeasuredWidth(), historyView.getMeasuredHeight());
        historyView.draw(c);

        //Saving to file
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            historyBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            historyView.getContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(out != null)
                out.close();
        }

        linearLayout.setOrientation(LinearLayout.VERTICAL);
    }

    /**
     * Class containing: plugin class, result image, and attributes set of history entry
     */
    public class MarvinHistoryEntry {

        private MarvinAttributes attributes;
        private MarvinImage image;
        private Class plugin;

        public MarvinHistoryEntry(MarvinAttributes attributes, MarvinImage image, Class pluginName) {
            if(attributes != null){
                this.attributes = attributes.clone();
            }
            if(image != null){
                this.image = image.clone();
            }
            this.plugin = pluginName;
        }

        public MarvinAttributes getAttributes() {
            return attributes;
        }

        public MarvinImage getImage() {
            return image;
        }

        public Class getPlugin() {
            return plugin;
        }
    }

}