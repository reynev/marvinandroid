/**
MarvinAndroid Project <2014>

Initial version by:

Marcin Piłat

site: https://bitbucket.org/reynev/marvinandroid

GPL
Copyright (C) <2014>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

package marvin.image;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.util.Arrays;

/**
 * Image object which speeds up operations on Image and provides some operations like convolution.
 *
 * @version 1.0 04.04.2014
 * @author reynev
 *
 * Based on MarvinImage from Marvin Project (marvinproject.sourceforge.net/)
 */
public class MarvinImage implements Cloneable {
	
	public final static int COLOR_MODEL_RGB 	= 0;
	public final static int COLOR_MODEL_BINARY 	= 1;
	
	// Definitions
	public final static int PROPORTIONAL = 0;
    public final static String DEFAULT_FILE_FORMAT = "png";

    // Image
	protected Bitmap image;
	
	// Array for RGB model
	protected int[] arrIntColor;
    // Array for binary model
	protected boolean[] arrBinaryColor;

	// Color Model
	protected int colorModel;
	
	// Format
	protected String formatName;

	// Dimension
	int width;
	int height;
	
	/**
	 * Constructor using a image in memory
	 * @param img Image
	 */
	public MarvinImage(Bitmap img){
        this(img, DEFAULT_FILE_FORMAT, COLOR_MODEL_RGB);
	}
	
	/**
	 * Constructor using a image in memory
	 * @param image Bitmap
	 * @param formatName Image format name
     * @param colorModel binary or RGB color model
	 */
	public MarvinImage(Bitmap image, String formatName, int colorModel){
		this.image =  image;
		this.formatName = formatName;
		width = image.getWidth();
		height = image.getHeight();
		this.colorModel = colorModel;
		updateColorArray();
	}

	/**
	 * Constructor to blank image, passing the size of image
     * @param w - image width
     * @param h - image height
	 */
	public MarvinImage(int w, int h){
        this(w, h, COLOR_MODEL_RGB, DEFAULT_FILE_FORMAT);
	}

    /**
     * Constructor to blank image
     * @param w - image width
     * @param h - image height
     * @param colorModel - binary or RGB color model
     */
    public MarvinImage(int w, int h, int colorModel){
        this(w, h, colorModel, DEFAULT_FILE_FORMAT);
    }

    /**
     * Constructor to blank image
     * @param w - image width
     * @param h - image height
     * @param colorModel - binary or RGB color model
     * @param formatName - Image format name
     */
	public MarvinImage(int w, int h, int colorModel, String formatName){
		this.colorModel = colorModel;
		this.formatName = formatName;
		setDimension(w, h);
	}

    /**
     * Returns cropped copy of this image
     * @param x - rectangle start position in x-axis
     * @param y - rectangle start position in y-axis
     * @param w - rectangle width
     * @param h - rectangle height
     * @return
     */
	public MarvinImage crop(int x, int y, int w, int h){
		return(new MarvinImage(Bitmap.createBitmap(image,x,y,w,h)));
	}

    /**
     * Updates color array from Bitmap
     */
	public void updateColorArray(){
        arrIntColor = new int[width*height];
        image.getPixels(arrIntColor, 0, width, 0, 0, width, height);
	}

    /**
     * Updates bitmap from color array
     */
	public void update(){
		int w = image.getWidth();
		switch(colorModel){
			case COLOR_MODEL_RGB:
                image.setPixels(arrIntColor, 0, width, 0, 0, width, height);
				break;
			case COLOR_MODEL_BINARY:
                image.setPixels(MarvinColorModelConverter.binaryToRgb(arrBinaryColor), 0, width, 0, 0, width, height);
				break;
		}
	}

    /**
     * Clears color array with new color (or with false if binary image)
     * @param color
     */
	public void clearImage(int color){
        switch(colorModel){
            case COLOR_MODEL_RGB:
                Arrays.fill(arrIntColor,color);
                break;
            case COLOR_MODEL_BINARY:
                Arrays.fill(arrBinaryColor,false);
                break;
        }
	}

	public Bitmap.Config getType(){
		return image.getConfig();
	}

	public int getColorModel(){
		return colorModel;
	}

    /**
     * Changes Image color model and optionally clears image.
     * For converting image @see MarvinColorModelConverter
     * @param cm - colorModel
     * @param clearImage - true if image has to be cleaned
     */
	public void setColorModel(int cm,boolean clearImage){
		colorModel = cm;
        if(clearImage){
            allocColorArray();
        }
	}
	
	/*
	 * @return image format name
	 */
	public String getFormatName(){
		return formatName;
	}

    /**
     * Clears Image and set new dimensions
     * @param w - image width
     * @param h - image height
     */
	public void setDimension(int w, int h){
		image = Bitmap.createBitmap(w,h, Bitmap.Config.ARGB_8888);
		width = w;
		height = h;
		allocColorArray();
	}

    /**
     * Allocates memory for color array
     */
	public void allocColorArray(){
		switch(colorModel){
			case COLOR_MODEL_RGB:
				arrBinaryColor = null;
				arrIntColor = new int[width*height];
				break;
			case COLOR_MODEL_BINARY:
				arrIntColor = null;
				arrBinaryColor = new boolean[width*height];
				break;
		}
	}

	/**
     * Should be encapsulated
	 * @return integer color array for the entire image.
	 */
	protected int[] getIntColorArray(){
		return arrIntColor;
	}
	
	/**
     * Should be encapsulated
	 *	Set the integer color array for the entire image.
	 **/
    protected void setIntColorArray(int[] arr){
		arrIntColor = arr;
	}

    /**
     * Copy color array from imgSource to imgDestine
     * @param imgSource - source MarvinImage
     * @param imgDestine - destination MarvinImage
     */
	public static void copyColorArray(MarvinImage imgSource, MarvinImage imgDestine){
		
		if(imgSource.getColorModel() != imgDestine.getColorModel()){
			throw new RuntimeException("copyColorArray(): Incompatible Images Color Model");
		}
		
		switch(imgSource.getColorModel()){
			case COLOR_MODEL_RGB:
				copyIntColorArray(imgSource, imgDestine);
				break;
			case COLOR_MODEL_BINARY:
				copyBinaryColorArray(imgSource, imgDestine);
				break;
		}
        imgDestine.width = imgSource.width;
        imgDestine.height = imgSource.height;
	}
	
	protected static void copyIntColorArray(MarvinImage imgSource, MarvinImage imgDestine){
		System.arraycopy(imgSource.getIntColorArray(), 0, imgDestine.getIntColorArray(), 0, imgSource.getWidth()*imgSource.getHeight());
	}
	
	protected static void copyBinaryColorArray(MarvinImage imgSource, MarvinImage imgDestine){
		System.arraycopy(imgSource.getBinaryColorArray(), 0, imgDestine.getBinaryColorArray(), 0, imgSource.getWidth()*imgSource.getHeight());
	}
	
	protected boolean[] getBinaryColorArray(){
		return arrBinaryColor;
	}
	
	public boolean getBinaryColor(int x, int y){
		return arrBinaryColor[y*width+x];
	}
	
	public void setBinaryColor(int x, int y, boolean value){
		arrBinaryColor[y*width+x] = value;
	}
	
	/**
	 * Gets the integer color composition for x, y position
     * @param x - pixel x-axis position
     * @param y - pixel y-axis position
	 * @return int		color
	 */
	public int getIntColor(int x, int y){
		return arrIntColor[y*width+x];
	}
	
	/**
	 *
     * @param x - pixel x-axis position
     * @param y - pixel y-axis position
	 * @return alpha component
	 */
	public int getAlphaComponent(int x, int y){
		return (arrIntColor[((y*width+x))]& 0xFF000000) >>> 24;
	}
	
	/**
	 * Gets the integer color component 0  in the x and y position
	 * @param x - pixel x-axis position
	 * @param y - pixel y-axis position
	 * @return for RGB model returns red
	 */	
	public int getIntComponent0(int x, int y){
		return (arrIntColor[((y*width+x))]& 0x00FF0000) >>> 16;
	}

	/**
	 * Gets the integer color component 1 in the x and y position
	 * @param x - pixel x-axis position
	 * @param y - pixel y-axis position
	 * @return for RGB model returns green
	 */	
	public int getIntComponent1(int x, int y){
		return (arrIntColor[((y*width+x))]& 0x0000FF00) >>> 8;
	}

	/**
	 * Gets the integer color component 2 in the x and y position
     * @param x - pixel x-axis position
     * @param y - pixel y-axis position
	 * @return for RGB model returns blue
	 */	
	public int getIntComponent2(int x, int y){
		return (arrIntColor[((y*width+x))] & 0x000000FF);
	}

	/**
	 * Returns the width 
	 * @return int	width
	 */
	public int getWidth(){
		return(image.getWidth());  	
	}

	/**
	 * Returns the height 
	 * @return int	height
	 */
	public int getHeight(){
		return(image.getHeight());  
	}

    /**
     * Returns if point in position (x,y) is in Image
     * @param x - pixel x-axis position
     * @param y - pixel y-axis position
     * @return false if point (x,y) is outside image
     */
	public boolean isValidPosition(int x, int y){
		if(x >= 0 && x < image.getWidth() && y >= 0 && y < getHeight()){
			return true;
		}
		return false;
	}

    /**
     * Sets color for pixel in point (x,y)
     * @param x - pixel x-axis position
     * @param y - pixel y-axis position
     * @param alpha - alpha value
     * @param color - color value
     */
	public void setIntColor(int x, int y, int alpha, int color){
		arrIntColor[((y*image.getWidth()+x))] = (alpha << 24) + color;
	}
	
	/**
	 * Sets the integer color composition in X an Y position
     * @param x - pixel x-axis position
     * @param y - pixel y-axis position
	 * @param color - color value
	 */
	public void setIntColor(int x, int y, int color){
		arrIntColor[((y*image.getWidth()+x))] = color;
	}

	/**
	 * Sets the integer color in X an Y position
	 * @param x 	position
	 * @param y 	position
	 * @param c0	component 0
	 * @param c1 	component 1
	 * @param c2 	component 2
	 */
	public void setIntColor(int x, int y, int c0, int c1, int c2){
		int alpha = (arrIntColor[((y*width+x))]& 0xFF000000) >>> 24;
		setIntColor(x,y,alpha,c0,c1,c2);
	}
	
	/**
	 * Sets the integer color in X an Y position
	 * @param x 	position
	 * @param y 	position
	 * @param c0	component 0
	 * @param c1 	component 1
	 * @param c2 	component 2
	 */
	public void setIntColor(int x, int y, int alpha, int c0, int c1, int c2){
		arrIntColor[((y*image.getWidth()+x))] = (alpha << 24)+
		(c0 << 16)+
		(c1 << 8)+
		c2;
	}

	/**
	 * Sets a new image, and updates color array
	 * @param img
	 */
	public void setBitmap(Bitmap img){
        if(image != null){
            image.recycle();
        }
		image = img;
		width = img.getWidth();
		height = img.getHeight();
		updateColorArray();		
	}

	/**
	 * @returns a Bitmap associated with the MarvinImage 
	 */
	public Bitmap getBitmap(){
		return image;
	}

    /**
     *
     * @return
     */
	public Bitmap getBitmapNoAlpha(){
		
		// Only for RGB images
		if(colorModel == COLOR_MODEL_RGB){
			int pixels = width*height;
			int[] pixelData = new int[pixels];
			for(int i=0; i<pixels; i++){
				pixelData[i] = arrIntColor[i] & 0x00FFFFFF;
			}
			Bitmap tmpImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            tmpImage.setPixels(arrIntColor, 0, width, 0, 0, width, height);
			return tmpImage;
		}
		return null;
	}
	
	/**
	 * Limits the color value between 0 and 255.
	 * @return int - the color value
	 */
	public int limit8bitsColor(int color){

		if(color > 255){
			color = 255;
			return(color);
		}

		if(color < 0){
			color = 0;
			return(color);
		}
		return color;
	}

	/**
	 * Convolution operator with 3x3 mask
	 * @return int[3] - results on every chanel
	 */
	public int[] Multi8p(int x, int y,int mask[][]){
		//a b c
		//d e f
		//g h i
		int aR = getIntComponent0(x-1,y-1);   int bR = getIntComponent0(x-1,y);  int cR = getIntComponent0(x-1,y+1);
		int aG = getIntComponent1(x-1,y-1);   int bG = getIntComponent1(x-1,y);  int cG = getIntComponent1(x-1,y+1);
		int aB = getIntComponent2(x-1,y-1);   int bB = getIntComponent2(x-1,y);  int cB = getIntComponent2(x-1,y+1);


		int dR = getIntComponent0(x,y-1);      int eR = getIntComponent0(x,y);   int fR = getIntComponent0(x,y+1);
		int dG = getIntComponent1(x,y-1);      int eG = getIntComponent1(x,y);   int fG = getIntComponent1(x,y+1);
		int dB = getIntComponent2(x,y-1);      int eB = getIntComponent2(x,y);   int fB = getIntComponent2(x,y+1);


		int gR = getIntComponent0(x+1,y-1);    int hR = getIntComponent0(x+1,y);   int iR = getIntComponent0(x+1,y+1);
		int gG = getIntComponent1(x+1,y-1);    int hG = getIntComponent1(x+1,y);   int iG = getIntComponent1(x+1,y+1);
		int gB = getIntComponent2(x+1,y-1);    int hB = getIntComponent2(x+1,y);   int iB = getIntComponent2(x+1,y+1);

		int rgb[] = new int[3];

		rgb[0] = ( (aR * mask[0][0]) + (bR * mask[0][1]) +(cR * mask[0][2])+
				(dR * mask[1][0]) + (eR * mask[1][1]) +(fR * mask[1][2])+
				(gR * mask[2][0]) + (hR * mask[2][1]) +(iR * mask[2][2]) );

		rgb[1] = ( (aG * mask[0][0]) + (bG * mask[0][1]) +(cG * mask[0][2])+
				(dG * mask[1][0]) + (eG * mask[1][1]) +(fG * mask[1][2])+
				(gG * mask[2][0]) + (hG * mask[2][1]) +(iG * mask[2][2]) );

		rgb[2] = ( (aB * mask[0][0]) + (bB * mask[0][1]) +(cB * mask[0][2])+
				(dB * mask[1][0]) + (eB * mask[1][1]) +(fB * mask[1][2])+
				(gB * mask[2][0]) + (hB * mask[2][1]) +(iB * mask[2][2]) );

		// return the value for all channel
		return (rgb);
	}

	/**
	 * Return a new instance of the Bitmap
	 * @return Bitmap
	 */
	public Bitmap getNewImageInstance(){
		return Bitmap.createBitmap(image);
	}

	/**
	 * Resize and return the image passing the new height and width
	 * @param height
	 * @param width
	 * @return
	 */
	public Bitmap getBitmap(int width, int height)
	{
		return Bitmap.createScaledBitmap(image,width,height,true);
	}
	
	/**
	 * Resize and return the image passing the new height and width, but maintains width/height factor
	 * @param height
	 * @param width
	 * @return
	 */
	 public Bitmap getBitmap(int width, int height, int type){
		 int	wDif, hDif,	fWidth = 0, fHeight = 0;

		 double	imgWidth,imgHeight;

		 double factor;
		 imgWidth = image.getWidth();
		 imgHeight = image.getHeight();
		

		 switch(type)
		 {
			 case PROPORTIONAL:
				 wDif = (int)imgWidth - width;
				 hDif = (int)imgHeight - height;
				 if(wDif > hDif){
					 factor = width/imgWidth;
				 }
				 else{
					 factor = height/imgHeight;
				 }
				 fWidth = (int)Math.floor(imgWidth*factor);
				 fHeight = (int)Math.floor(imgHeight*factor);
				 break;
		 }
		 return getBitmap(fWidth, fHeight);
	 }
	/**
	 * Resize the image passing the new height and width
	 * @param w
	 * @param h
	 * @return
	 */
	public void resize(int w, int h)
	{
        width = w;
        height = h;
        image = Bitmap.createScaledBitmap(image,width,height,true);
		updateColorArray();
	}
	
	/**
     * Clones Image
	 */
	@Override
    public MarvinImage clone() {
        MarvinImage newMarvinImg = new MarvinImage(image.copy(Bitmap.Config.ARGB_8888,true));
		return newMarvinImg;
	}
	/**
	 * Multiple of gradient windows per masc relation of x y
     *
	 * @return double result of multiply
	 */
	/*public double multi8p(int x, int y,double masc){
		int aR = getIntComponent0(x-1,y-1);     int bR = getIntComponent0(x-1,y);    int cR = getIntComponent0(x-1,y+1);
		int aG = getIntComponent1(x-1,y-1);   int bG = getIntComponent1(x-1,y);  int cG = getIntComponent1(x-1,y+1);
		int aB = getIntComponent1(x-1,y-1);   int bB = getIntComponent1(x-1,y);  int cB = getIntComponent1(x-1,y+1);

		
		int dR = getIntComponent0(x,y-1);        int eR = getIntComponent0(x,y);     int fR = getIntComponent0(x,y+1);
		int dG = getIntComponent1(x,y-1);      int eG = getIntComponent1(x,y);   int fG = getIntComponent1(x,y+1);
		int dB = getIntComponent1(x,y-1);      int eB = getIntComponent1(x,y);   int fB = getIntComponent1(x,y+1);

		
		int gR = getIntComponent0(x+1,y-1);      int hR = getIntComponent0(x+1,y);     int iR = getIntComponent0(x+1,y+1);
		int gG = getIntComponent1(x+1,y-1);    int hG = getIntComponent1(x+1,y);   int iG = getIntComponent1(x+1,y+1);
		int gB = getIntComponent1(x+1,y-1);    int hB = getIntComponent1(x+1,y);   int iB = getIntComponent1(x+1,y+1);

		double rgb = 0;

		rgb = ( (aR * masc) + (bR * masc) +(cR * masc)+
				(dR * masc) + (eR * masc) +(fR * masc)+
				(gR * masc) + (hR * masc) +(iR * masc) ); 

		return(rgb);

	}*/
	
	public int boundRGB(int rgb){
        return limit8bitsColor(rgb);
	}
	
	/**
	 * Bresenhams Line Drawing implementation
	 */
	public void drawLine(int x0, int y0, int x1, int y1, int r, int g, int b) {
		int colorRGB = Color.rgb(r,g,b);
		int dy = y1 - y0;
		int dx = x1 - x0;
		int stepx, stepy;
		int fraction;
		
		
		if (dy < 0) { dy = -dy; stepy = -1; 
		} else { stepy = 1; 
		}
	 	if (dx < 0) { dx = -dx; stepx = -1; 
		} else { stepx = 1; 
		}
		dy <<= 1; 							// dy is now 2*dy
		dx <<= 1; 							// dx is now 2*dx
	 
		setIntColor(x0, y0, colorRGB);

		if (dx > dy) {
			fraction = dy - (dx >> 1);	// same as 2*dy - dx
			while (x0 != x1) {
				if (fraction >= 0) {
					y0 += stepy;
					fraction -= dx; 		// same as fraction -= 2*dx
				}
				x0 += stepx;
	   		fraction += dy; 				// same as fraction -= 2*dy
	   		setIntColor(x0, y0, colorRGB);
			}
		} else {
			fraction = dx - (dy >> 1);
			while (y0 != y1) {
				if (fraction >= 0) {
					x0 += stepx;
					fraction -= dy;
				}
			y0 += stepy;
			fraction += dx;
			setIntColor(x0, y0, colorRGB);
			}
		}
	} 

	
	/**
	 * Draws a rectangle in the image. Its useful for debugging purposes.
	 * @param x		rects start position in x-axis
	 * @param y		rects start positioj in y-axis
	 * @param w		rects width
	 * @param h		rects height
	 * @params r g b		rects color
	 */
	public void drawRect(int x, int y, int w, int h, int r, int g, int b){
		int color = Color.rgb(r,g,b);
		for(int i=x; i<x+w; i++){
			setIntColor(i, y, color);
			setIntColor(i, y+(h-1), color);
		}
		
		for(int i=y; i<y+h; i++){
			setIntColor(x, i, color);
			setIntColor(x+(w-1), i, color);
		}
	}
	
	/**
	 * Fills a rectangle in the image.
	 * @param x		rects start position in x-axis
	 * @param y		rects start positioj in y-axis
	 * @param w		rects width
	 * @param h		rects height
	 */
	public void fillRect(int x, int y, int w, int h, int r, int g, int b){
		int color = Color.rgb(r,g,b);
		for(int i=x; i<x+w; i++){
			for(int j=y; j<y+h; j++){
				setIntColor(i,j,color);
			}
		}
	}
	
	/**
	 * Compare two MarvinImage objects
	 * @param obj	object to be compared. MarvinImage object is expected.
	 */
	public boolean equals(Object obj){
		MarvinImage img = (MarvinImage) obj;
		int[] l_arrColor = img.getIntColorArray();
		
		if(getWidth() != img.getWidth() || getHeight() != img.getHeight()){
			return false;
		}
		
		for(int l_cont=0; l_cont<getHeight(); l_cont++){
			if(arrIntColor[l_cont] != l_arrColor[l_cont]){
				return false;
			}
		}	
		return true;
	}
}